/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

const Main = imports.ui.main;
const SessionMode = imports.ui.sessionMode;

class Extension {
    constructor() {
    }

    enable() {
      let centerBox = Main.panel._centerBox;
      let leftBox   = Main.panel._leftBox;
      let dateMenu  = Main.panel.statusArea['dateMenu'];
      let children  = centerBox.get_children();
      
      if ( children.indexOf(dateMenu.container) != -1 ) {
        centerBox.remove_actor(dateMenu.container);

        children = leftBox.get_children();
        leftBox.insert_child_at_index(dateMenu.container, children.length-1);
      }
    }

    disable() {
      let centerBox = Main.panel._centerBox;
      let leftBox   = Main.panel._leftBox;
      let dateMenu = Main.panel.statusArea['dateMenu'];
      let children = leftBox.get_children();

      // only move the clock back if it's in the right box
      if ( children.indexOf(dateMenu.container) != -1 ) {
          leftBox.remove_actor(dateMenu.container);
          centerBox.add_actor(dateMenu.container);
      }
    }
}

function init() {
    return new Extension();
}
